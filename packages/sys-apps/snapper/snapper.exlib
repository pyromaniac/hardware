# Copyright 2013-2018 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.13 1.12 ] ]

if ever is_scm ; then
    SCM_REPOSITORY="git://github.com/openSUSE/${PN}.git"
    require scm-git
else
    DOWNLOADS="ftp://ftp.suse.com/pub/projects/${PN}/${PNV}.tar.bz2"
fi

export_exlib_phases src_prepare src_install pkg_postinst

SUMMARY="filesystem snapshot tool"
DESCRIPTION="
Snapper is a tool for Linux filesystem snapshot management. Apart from the obvious
creation and deletion of snapshots, it can compare snapshots and revert differences
between snapshots. In simple terms, this allows root and non-root users to view
older versions of files and revert changes.
"
HOMEPAGE="http://${PN}.io/"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.5
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/boost[>=1.55.0]
        dev-libs/libxml2
        sys-apps/acl
        sys-apps/dbus
        sys-apps/util-linux
        sys-fs/btrfs-progs
        sys-libs/pam
    run:
        sys-fs/lvm2
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --sbindir=/usr/$(exhost --target)/bin
    --libdir=/usr/$(exhost --target)/lib
    --libexecdir=/usr/$(exhost --target)/libexec
    --enable-btrfs
    --enable-ext4
    --enable-lvm
    --enable-pam
    --enable-rollback
    --enable-xattrs
    --disable-zypp
    --with-conf=/etc/snapper
)

snapper_src_prepare() {
    edo sed -i -e "s:/lib/\(pam_snapper\):/usr/$(exhost --target)/lib/\1:g" \
        doc/pam_snapper.xml.in \
        scripts/Makefile.am

    edo sed -i -e "s:/lib/\(snapper\):/$(exhost --target)/lib/\1:g" client/Makefile.am
    edo sed -i -e "/securelibdir/s:=.*:= /usr/$(exhost --target)/lib/security:" pam/Makefile.am
    edo sed -i -e "s:/usr/lib/systemd/system:${SYSTEMDSYSTEMUNITDIR}:g" data/Makefile.am

    autotools_src_prepare
}

snapper_src_install() {
    default

    edo cp -a "${IMAGE}"/usr/usr/* "${IMAGE}"/usr/
    edo rm -r "${IMAGE}"/usr/usr

    # Remove the zypp crap
    [[ -f ${IMAGE}/etc/snapper/zypp-plugin.conf ]] && edo rm "${IMAGE}"/etc/snapper/zypp-plugin.conf

    # Move the cron files
    edo mv "${IMAGE}"/etc/cron* "${IMAGE}"/usr/share/doc/${PNVR}/

    # Create an empty config
    edo touch "${IMAGE}"/etc/snapper/snapper

    # Needed for snapper create-config
    keepdir /etc/snapper/configs
}

snapper_pkg_postinst() {
    elog "You'll find example cron jobs for house-keeping in /usr/share/doc/${PNVR}"
}

