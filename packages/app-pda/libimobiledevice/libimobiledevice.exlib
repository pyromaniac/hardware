# Copyright 2009, 2010 Ingmar Vanhassel
# Copyright 2010-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

AT_M4DIR=( m4 )
require github [ user=libimobiledevice release=${PV} suffix=tar.bz2 ]
ever is_scm && require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
require option-renames [ renames=[ 'gnutls providers:gnutls' ] ]
require python [ blacklist=none multibuild=false with_opt=true ]

export_exlib_phases src_configure src_install

SUMMARY="Library that talks the native Apple USB protocols iDevices use"
DESCRIPTION="
${PN} is a software library that talks the native Apple USB protocols
that iDevices use.
Unlike other projects, \"${PN}\" does not depend on using any existing
\".dll\" or \".so\" libraries from Apple.
"
HOMEPAGE+=" https://${PN}.org"

LICENCES="GPL-2 LGPL-2.1"
SLOT="1.0"
MYOPTIONS="
    ( providers:
        gnutls
        ( openssl libressl ) [[ *description = [ Use libssl instead of GnuTLS (might not work properly!) ] ]]
    ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        python? ( dev-python/Cython[>=0.17.0] )
    build+run:
        app-pda/libusbmuxd:2.0[>=2.0.2]
        dev-libs/glib:2[>=2.14.1]
        dev-libs/libgcrypt
        dev-libs/libplist:2.0[>=2.2][python?][python_abis:*(-)?]
        providers:gnutls? (
            dev-libs/gnutls[>=2.2.0]
            dev-libs/libtasn1[>=1.1]
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=0.9.8] )
        !${CATEGORY}/${PN}:0 [[
            description = [ 1.0 slot move ]
            resolution = uninstall-blocked-before
        ]]
"

libimobiledevice_src_configure() {
    local myconf=(
        --disable-static
    )

    option providers:gnutls && myconf+=( --disable-openssl )
    option python || myconf+=( --without-cython )

    econf "${myconf[@]}"
}

libimobiledevice_src_install() {
    default

    keepdir /var/lib/lockdown
    edo chmod 777 "${IMAGE}"/var/lib/lockdown

    option python && python_bytecompile
}

