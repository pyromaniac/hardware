# Copyright 2009,2014 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV="v${PV}"
WORK="${WORKBASE}/${PN}-${MY_PV}"

require gitlab [ prefix=https://gitlab.freedesktop.org user=libfprint new_download_scheme=true ]
require meson udev-rules

SUMMARY="Core library for the fprint stack"
HOMEPAGE="https://fprint.freedesktop.org"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    fprint_drivers:
        (
            aes1610 [[ description = [ AuthenTec AES1610 ] ]]
            aes1660 [[ description = [ AuthenTec AES1660 ] ]]
            aes2501 [[ description = [ AuthenTec AES2501 ] ]]
            aes2550 [[ description = [ AuthenTec AES2550/AES2810 ] ]]
            aes2660 [[ description = [ AuthenTec AES1660 ] ]]
            aes3500 [[ description = [ AuthenTec AES3500 ] ]]
            aes4000 [[ description = [ AuthenTec AES4000 ] ]]
            elan [[ description = [ ElanTech Fingerprint Sensor ] ]]
            elanmoc [[ description = [ Elan MOC Sensors ] ]]
            elanspi [[ description = [ ElanTech Fingerprint Sensor ] ]]
            etes603 [[ description = [ EgisTec ES603 ] ]]
            egis0570 [[ description = [ EgisTec 0570 ] ]]
            goodixmoc [[ description = [ Goodix Moc ] ]]
            fpcmoc [[ description = [ Fingerprint Cards AB ] ]]
            nb1010 [[ description = [ Next Biometrics ] ]]
            synaptics [[ description = [ Synaptics Sensors ] ]]
            upeksonly [[ description = [ UPEK TouchStrip sensor-only ] ]]
            upektc [[ description = [ UPEK TouchChip ] ]]
            upektc_img [[ description = [ Upek TouchChip Fingerprint Coprocessor ] ]]
            upekts [[ description = [ UPEK TouchStrip ] ]]
            uru4000 [[ description = [ Digital Personal U.are.U 4000/4000B ] ]]
            vcom5s [[ description = [ Veridicom 5thSense ] ]]
            vfs0050 [[ description = [ Validity VFS0050+ ] ]]
            vfs101 [[ description = [ Validity VFS101 ] ]]
            vfs301 [[ description = [ Validity VFS301/VFS300 ] ]]
            vfs5011 [[ description = [ Validity VFS5011 ] ]]
            vfs7522 [[ description = [ Validity VFS7552 ] ]]
    ) [[ number-selected = at-least-one ]]
    providers: ( eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        dev-libs/glib:2[>=2.56]
        dev-libs/libgusb[>=0.2.0]
        x11-libs/cairo
        fprint_drivers:aes3500? ( x11-libs/pixman:1 )
        fprint_drivers:aes4000? ( x11-libs/pixman:1 )
        fprint_drivers:elanspi? ( gnome-desktop/libgudev )
        fprint_drivers:uru4000? ( dev-libs/nss )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1 )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd[>=248] )
"

src_configure() {
    for driver in ${FPRINT_DRIVERS} ; do
        drivers+=( ${driver} )
    done

    local meson_params=(
        -Ddrivers=$(IFS=, ; echo "${drivers[*]}")
        -Dgtk-examples=false
        -Dudev_hwdb=enabled
        -Dudev_hwdb_dir=${UDEVHWDBDIR}
        -Dudev_rules=enabled
        -Dudev_rules_dir=${UDEVRULESDIR}
        $(meson_switch gobject-introspection introspection)
        $(meson_switch gtk-doc doc)
    )

    meson_src_configure "${meson_params[@]}"
}

src_test() {
    esandbox allow_net --bind "unix:${TEMP}/umockdev.*/event*";
    esandbox allow_net --bind "unix:${TEMP}/umockdev.*/ioctl/_default";
    esandbox allow_net --bind "unix:${TEMP}/umockdev.*/ioctl/dev/bus/usb/**";
    meson_src_test
    esandbox disallow_net --bind "unix:${TEMP}/umockdev.*/ioctl/dev/bus/usb/**";
    esandbox disallow_net --bind "unix:${TEMP}/umockdev.*/ioctl/_default";
    esandbox disallow_net --bind "unix:${TEMP}/umockdev.*/event*";
}

