# Copyright 2013-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=Argyll_V${PV}

require flag-o-matic \
    jam \
    udev-rules [ udev_files=[ usb/55-Argyll.rules ] ]

SUMMARY="ICC compatible color management system (autotools version)"
DESCRIPTION="
The Argyll color management system supports accurate ICC profile creation for
acquisition devices, CMYK printers, film recorders and calibration and profiling
of displays.

Spectral sample data is supported, allowing a selection of illuminants observer
types, and paper fluorescent whitener additive compensation. Profiles can also
incorporate source specific gamut mappings for perceptual and saturation
intents. Gamut mapping and profile linking uses the CIECAM02 appearance model,
a unique gamut mapping algorithm, and a wide selection of rendering intents. It
also includes code for the fastest portable 8 bit raster color conversion
engine available anywhere, as well as support for fast, fully accurate 16 bit
conversion. Device color gamuts can also be viewed and compared using a VRML
viewer.
"
HOMEPAGE="https://www.argyllcms.com"
DOWNLOADS="${HOMEPAGE}/${MY_PNV}_src.zip"

LICENCES="AGPL-3 GPL-3 MIT"
SLOT="0"
MYOPTIONS="
    doc
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/unzip
    build+run:
        media-libs/libpng:=
        media-libs/tiff:=
        sys-libs/zlib
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXinerama
        x11-libs/libXrandr
        x11-libs/libXScrnSaver
        x11-libs/libXxf86vm
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
    recommendation:
        sys-apps/colord [[
            description = [ Installs udev rules required for using measurement devices ]
        ]]
"

WORK=${WORKBASE}/${MY_PNV}

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( log.txt ttbd.txt )

src_prepare() {
    default

    append-flags "-DUNIX -D_THREAD_SAFE"

    # respect LDFLAGS
    echo "LINKFLAGS += ${LDFLAGS} ;" >> Jamtop

    # respect AR
    export AR="${AR} rc"

    # respect CFLAGS
    edo sed \
        -e 's:CCFLAGS:CFLAGS:g' \
        -i Jambase

    # do not install share into prefix
    edo sed \
        -e 's:$(PREFIX)/$(REFSUBDIR):/$(REFSUBDIR):g' \
        -i {gamut,icc,profile,scanin,spectro,target}/Jamfile \
        -i Jamtop
}

src_compile() {
    ejam -dx -q -fJambase -sDESTDIR="${IMAGE}" -sPREFIX=/usr/$(exhost --target) -sREFSUBDIR=usr/share/argyllcms/ref -j${EXJOBS:-1} all
}

src_install() {
    ejam -dx -q -fJambase -sDESTDIR="${IMAGE}" -sPREFIX=/usr/$(exhost --target) -sREFSUBDIR=usr/share/argyllcms/ref -j${EXJOBS:-1} install

    edo rm "${IMAGE}"/usr/$(exhost --target)/bin/License.txt

    install_udev_files

    emagicdocs

    if option doc; then
        insinto /usr/share/doc/${PNVR}/html
        doins -r "${WORK}"/doc/*
    fi
}

