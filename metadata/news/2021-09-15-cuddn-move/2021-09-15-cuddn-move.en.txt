Title: dev-libs/cuddn has been renamed to dev-libs/cudnn
Author: Timo Gurr <tgurr@exherbo.org>
Content-Type: text/plain
Posted: 2021-09-15
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-libs/cuddn

Please install dev-libs/cudnn and *afterwards* uninstall dev-libs/cuddn.

1. Take note of any packages depending on dev-libs/cuddn:
cave resolve \!dev-libs/cuddn

2. Install dev-libs/cudnn:
cave resolve dev-libs/cudnn -x1

3. Re-install the packages from step 1.

4. Uninstall dev-libs/cuddn:
cave resolve \!dev-libs/cuddn -x

Do it in *this* order or you'll potentially *break* your system.
